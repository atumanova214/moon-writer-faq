# Основы Markdown

## Краткое руководство

Абзацы создаются при помощи пустой строки. Если вокруг текста сверху и снизу есть пустые строки, то текст превращается в абзац.

Чтобы сделать перенос строки вместо абзаца,  
нужно поставить два пробела в конце предыдущей строки.

Заголовки отмечаются диезом `#` в начале строки, от одного до шести.

Синтаксис:

`# Заголовок первого уровня`  
`## Заголовок h2`  
`### Заголовок h3`  
`#### Заголовок h4`  
`##### Заголовок h5`  
`###### Заголовок h6`  

Результат: 

# Заголовок первого уровня
## Заголовок h2
### Заголовок h3
#### Заголовок h4
##### Заголовок h5
###### Заголовок h6

# Списки
### Неупорядоченный список
Для разметки неупорядоченных списков можно использовать или `*`, или `-`, или `+`:



Синтаксис:

`- элемент 1`  
`- элемент 2`  
`- элемент ...`

Результат: 

- элемент 1
- элемент 2
- элемент ...

### Упорядоченный список:

Синтаксис:

`1. элемент 1`  
`2. элемент 2`  
`3. элемент 3`  
`4. Donec sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse id sem consectetuer libero luctus adipiscing.`

Результат: 

1. элемент 1
2. элемент 2
3. элемент 3
4. Donec sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse id sem consectetuer libero luctus adipiscing.

На самом деле не важно как в сыром виде пронумерованы пункты, главное, чтобы перед элементом списка стояла цифра (любая) с точкой. Можно сделать и так:

Синтаксис:

`0. элемент 1`  
`0. элемент 2`  
`0. элемент 3`  
`0. элемент 4`  

Результат: 

0. элемент 1
0. элемент 2
0. элемент 3
0. элемент 4

# Цитаты

Цитаты оформляются как в емейлах, с помощью символа `>`.

Синтаксис:

`> This is a blockquote with two paragraphs. Lorem ipsum dolor sit amet,`  
`> consectetuer adipiscing elit. Aliquam hendrerit mi posuere lectus.`  
`> Vestibulum enim wisi, viverra nec, fringilla in, laoreet vitae, risus.`  
`>`  
`> Donec sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse`  
`> id sem consectetuer libero luctus adipiscing.`

Результат: 

> This is a blockquote with two paragraphs. Lorem ipsum dolor sit amet,
> consectetuer adipiscing elit. Aliquam hendrerit mi posuere lectus.
> Vestibulum enim wisi, viverra nec, fringilla in, laoreet vitae, risus.
>
> Donec sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse
> id sem consectetuer libero luctus adipiscing.

Или более ленивым способом, когда знак `>` ставится перед каждым элементом цитаты, будь то абзац, заголовок или пустая строка:

Синтаксис:

`> This is a blockquote with two paragraphs. Lorem ipsum dolor sit amet,`  
`consectetuer adipiscing elit. Aliquam hendrerit mi posuere lectus.`  
`Vestibulum enim wisi, viverra nec, fringilla in, laoreet vitae, risus.`   
`>`  
`> Donec sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse`  
`id sem consectetuer libero luctus adipiscing.`  

Результат: 

> This is a blockquote with two paragraphs. Lorem ipsum dolor sit amet,
consectetuer adipiscing elit. Aliquam hendrerit mi posuere lectus.
Vestibulum enim wisi, viverra nec, fringilla in, laoreet vitae, risus.
>
> Donec sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse
id sem consectetuer libero luctus adipiscing.


В цитаты можно помещать всё что угодно, в том числе вложенные цитаты:

Синтаксис:

`> ## This is a header.`  
`>`  
`> 1.   This is the first list item.`  
`> 2.   This is the second list item.`  
`>`  
`> > Вложенная цитата.`  
`>`  
`> Here's some example code:`  
`>`  
`>       context.resources.configuration.locales.get(0)`  

Результат: 

> ## This is a header.
>
> 1.   This is the first list item.
> 2.   This is the second list item.
>
> > Вложенная цитата.
>
> Here's some example code:
>
>       context.resources.configuration.locales.get(0)



# Исходный код

Синтаксис:

<p>`fun main(args: Array<String>) {println("Hello World!")}`</p>

Результат:

`fun main(args: Array<String>) {println("Hello World!")}`

# Горизонтальная черта

`hr` создается тремя звездочками или тремя дефисами.

Синтаксис:

`***`

Результат (серая полоса):

***

# Ссылки

Синтаксис:

`Это встроенная [ссылка с title элементом](http://example.com/link "Я ссылка"). Это — [без title](http://example.com/link).`  

`А вот [пример][1] [нескольких][2] [ссылок][id] с разметкой как у сносок. Прокатит и [короткая запись][] без указания id.`  

`[1]: http://example.com/ "Optional Title Here"`  
`[2]: http://example.com/some`  
`[id]: http://example.com/links (Optional Title Here)`  
`[короткая запись]: http://example.com/short`  

Результат:

Это встроенная [ссылка с title элементом](http://example.com/link "Я ссылка"). Это — [без title](http://example.com/link).

А вот [пример][1] [нескольких][2] [ссылок][id] с разметкой как у сносок. Подойдёт и [короткая запись][] без указания id.

[1]: http://example.com/ "Optional Title Here"
[2]: http://example.com/some
[id]: http://example.com/links (Optional Title Here)
[короткая запись]: http://example.com/short

Вынос длинных урлов из предложения способствует сохранению читабельности исходника. Сноски можно располагать в любом месте документа.




# Emphasis

Выделять слова можно при помощи `*` и `_`. Одним символ для наклонного текста, два символа для жирного текста, три — для наклонного и жирного одновременно.

Синтаксис:

`Например, это _italic_ и это тоже *italic*. А вот так уже __strong__, и так тоже **strong**. А так ***жирный и наклонный*** одновременно.`

Результат:

Например, это _italic_ и это тоже *italic*. А вот так уже __strong__, и так тоже **strong**. А так ***жирный и наклонный*** одновременно.

# Зачеркивание

Две тильды `~` до и после текста.

Синтаксис:

`~~Зачеркнуто~~`

Результат:

<s>Зачеркнуто</s>

# Подчеркивание

Текст нужно обернуть HTML тегами `<u>` и `</u>`

Синтаксис:

`<u>Подчеркнутый</u>`

Результат:

<u>Подчеркнутый</u>

# TODO список

Синтаксис:

`- [x] элемент 1`  
`- [ ] элемент 2`  
`- [ ] элемент 3`  