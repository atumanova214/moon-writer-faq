# Добро пожаловать в Moon Writer FAQ

Обратите внимание, что это предварительная версия FAQ, она будет расширяться и в неё будут вноситься правки.  
Вы так же можете принять участиве в разработке этого ресурса, вся информация находится в разделе [исходных файлов FAQ.](https://wimank.gitlab.io/apps_web_pages/moon_writer/faq/ru/source_files_faq/)

![](img/moon_writer_long_logo.png) 

Moon Writer – это минималистичный редактор текста с поддержкой Markdown.

# С чего начать?
Начнём с определения слова Markdown.

Markdown (произносится маркда́ун) — облегчённый язык разметки, созданный с целью написания наиболее читаемого и удобного для правки текста.

«Язык разметки» — это просто набор соглашений, правил.

Допустим, что вы общаетесь с другом по СМС. В них нельзя сделать текст жирным или наклонным. Вы договариваетесь с другом: если я пишу ?что-то? вот так, между знаками вопроса, то считай, что это наклонный текст. А если я пишу !что-то! между восклицательных знаков, то считай, что это жирный текст. Вы придумали правила.

Markdown — это набор подобных правил.

Правила понятны разным программам и сайтам в том числе и Moon Writer. Это значит, что вы можете писать в приложении текст по правилам Markdown, а после перехода в режим предварительного просмотра (preview) разметка станет реальной: текст в одинарных звездочках станет наклонным, текст в двойных звездочках станет жирным и так далее.

# Что дальше?
Далее рекомендуется ознакомиться с интерфейсом программы.