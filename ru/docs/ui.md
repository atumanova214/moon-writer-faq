# Главный экран приложения
![](img/1screen.png)

При запуске приложения пользователь попадает на главный экран.  
Вверху слева расположена кнопка меню, справа кнопка поиска по файлам и папкам.  
Круглая кнопка с плюсом отвечает за добавление новых папок и файлов.  

# Добавление новых папок и файлов
Для того, чтобы создать новый файл или папку, нажмите на круглую кнопку со значком плюса, далее откроется окно в котором будет предложено выбрать, что хочет создать пользователь.
![](img/file_or_folder.png)

# Диалог создания файла
![](img/file_create_dialog.png)

Чтобы создать новый файл введите его имя и нажмите кнопку создать.  
Автонаименование создаёт файл, имя которого будет, состоять из даты и времени (пригодится для быстрых заметок).  
Так же на выбор можно создать .txt или .md файл, отметьте галочкой нужное расширение и создайте файл введя его имя или нажмите кнопку автонаименования.  

# Диалог создания папки
![](img/folder_create_dialog.png)

Чтобы создать новую папку введите её имя и нажмите кнопку создать.  
Автонаименование создаёт папку, имя которой, будет состоять из даты и времени.  