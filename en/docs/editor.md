# Text editor
![](img/editor.png)

The text editor allows you to write and edit the user's text using the Markdown rules or without using it (Markdown can be disabled in the settings).  
Before the user is a text entry window and formatting panel. These are the main elements in the editor.

# Format Panel
The formatting panel is a scrollable element on which the buttons are located (their order can be changed in the settings) inserts of various Markdown syntax structures.

Let's go through the functions of the formatting panel:

![](img/formatting_panel/undo-solid.svg) - cancel  
![](img/formatting_panel/redo-solid.svg) - repeat  
![](img/formatting_panel/search-solid.svg) - search by text  
![](img/formatting_panel/bold-solid.svg) - bold text  

With a long tap on the icon, you can open an additional choice of bold text syntax for WhatsApp and tap on them to set the necessary characters for the current editing session.  
![](img/formatting_panel/long_click_bold.png)  
The additional selection buttons can be closed by selecting the required characters or by long tap on the bold text button.  

![](img/formatting_panel/italic-solid.svg) - italics text  
With a long tap on the icon, you can open an additional choice of oblique text syntax for Telegram and WhatsApp  
![](img/formatting_panel/long_click_italic.png)  
The additional selection buttons can be closed by selecting the required characters or by a long tap on the oblique text button.

![](img/formatting_panel/strikethrough-solid.svg) - strikethrough text  
![](img/formatting_panel/underline-solid.svg) - underlined text  
![](img/formatting_panel/link-solid.svg) - link  
![](img/formatting_panel/code-solid.svg) - code  
![](img/formatting_panel/heading-solid.svg) - heading  
The dialog for selecting the size of the header will open, the largest = 1, that is #

![](img/formatting_panel/quote-right-solid.svg) - quote  
![](img/formatting_panel/list-ol-solid.svg) - numbered list  
![](img/formatting_panel/check-square-solid.svg) - to-do list

# How to hide the formatting panel
![](img/formatting_panel/editor_hide_fp.png)

To hide the formatting panel and top information elements, slide your finger from the left **or** right edge of the screen to return the formatting panel and information elements repeat the operation again.