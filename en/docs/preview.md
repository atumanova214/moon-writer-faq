# Preview
![](img/preview.png)

Preview is used to evaluate the result after writing the text and its formatting in the editor.
**Text from Markdown is displayed only in the preview window!**

Located on the top:

- file name
- button **share**
- button **to the beginning of the file**
- button **at the end of the file**

In the lower right corner there is a button to go to the text editor.