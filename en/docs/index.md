# Welcome to the Moon Writer FAQ

Please note that this is a preliminary version of the FAQ, it will be expanded and edits will be made to it.
You can also take part in the development of this resource, all information is in the [source files FAQ](https://wimank.gitlab.io/apps_web_pages/moon_writer/faq/ru/source_files_faq/) section

![](img/moon_writer_long_logo.png)

Moon Writer is a minimal text editor with Markdown support.

# Where to begin?
Let's start with the definition of the word Markdown.

Markdown - lightweight markup language, created to write the most readable and easy to edit text.

"Markup language" is just a set of agreements, rules.

Let's say that you communicate with a friend via SMS. They can not make the text bold or oblique. You agree with a friend: if I write ?something? like this, between the question marks, consider that it is an oblique text. And if I write !something! between exclamation marks, then consider it to be a bold text. You came up with the rules.

Markdown is a set of similar rules.

The rules are understandable to different programs and sites, including Moon Writer. This means that you can write text according to the Markdown rules in the application, and after switching to the preview mode (preview), the markup will become real: the text in single asterisks will become oblique, the text in double asterisks will become bold, and so on.

# What's next?
Further, it is recommended to get acquainted with the program interface.