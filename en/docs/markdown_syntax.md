# Markdown Basics

## Quick Start Guide

Paragraphs are created using a blank line. If there are blank lines around the text above and below, the text becomes a paragraph.

To make a line break instead of a paragraph,
you need to put two spaces at the end of the previous line.

Headings are marked with a sharp `#` at the beginning of a line, from one to six.

Syntax:

`# h1 Header`  
`## h2 Header`  
`### h3 Header`  
`#### h4 Header`  
`##### h5 Header`  
`###### h6 Header`

Result:

# h1 Header
## h2 Header
### h3 Header
#### h4 Header
##### h5 Header
###### h6 Header

# Lists
### Unordered List
You can use either `*`, or `-`, or `+` to mark unordered lists:



Syntax:

`- item 1`  
`- item 2`  
`- item ...`

Result:

- item 1
- item 2
- item ...

### Ordered list:

Syntax:

`1. item 1`  
`2. item 2`  
`3. item 3`  
`4. Donec sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse id sem consectetuer libero luctus adipiscing.`

Result:

1. item 1
2. item 2
3. item 3
4. Done sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse id sem consectetuer libero luctus adipiscing.

In fact, it doesn’t matter how raw the points are numbered, the main thing is that there should be a number (any) with a dot before the list item. You can do this:

Syntax:

`0. item 1`  
`0. item 2`  
`0. item 3`  
`0. item 4`

Result:

0. item 1
0. item 2
0. item 3
0. item 4

# Quotes

Quotes are drawn up as in emails, using the `>` symbol.

Syntax:

`> This is a blockquote with two paragraphs. Lorem ipsum dolor sit amet, `  
`> consectetuer adipiscing elit. Aliquam hendrerit mi posuere lectus.`  
`> Vestibulum enim wisi, viverra nec, fringilla in, laoreet vitae, risus.`  
`>`  
`> Donec sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse`  
`> id sem consectetuer libero luctus adipiscing.`  

Result:

> This is a blockquote with two paragraphs. Lorem ipsum dolor sit amet,
> consectetuer adipiscing elit. Aliquam hendrerit mi posuere lectus.
> Vestibulum enim wisi, viverra nec, fringilla in, laoreet vitae, risus.  
>  
> Donec sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse
> id sem consectetuer libero luctus adipiscing.  

Or in a more lazy way, when the `>` sign is placed before each element of the quotation, whether it is a paragraph, a heading, or an empty line:

Syntax:

`> This is a blockquote with two paragraphs. Lorem ipsum dolor sit amet, `  
`consectetuer adipiscing elit. Aliquam hendrerit mi posuere lectus.`  
`Vestibulum enim wisi, viverra nec, fringilla in, laoreet vitae, risus.`  
`>`  
`> Donec sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse`  
`id sem consectetuer libero luctus adipiscing.`

Result:

> This is a blockquote with two paragraphs. Lorem ipsum dolor sit amet,  
consectetuer adipiscing elit. Aliquam hendrerit mi posuere lectus.  
Vestibulum enim wisi, viverra nec, fringilla in, laoreet vitae, risus.  
>  
> Donec sit amet nisl. Aliquam semper ipsum sit amet velit. Suspendisse  
id sem consectetuer libero luctus adipiscing.


Anything can be placed in quotes, including embedded quotes:

Syntax:

`> ## This is a header.`  
`>`  
`> 1. This is the first list item.`  
`> 2. This is the second list item.`  
`>`  
`> > Nested quote.`  
`>`  
`> Here's some example code:`  
`>`  
`> context.resources.configuration.locales.get (0)`

Result:

> ## This is a header.
>
> 1. This is the first list item.
> 2. This is the second list item.
>
> > Nested quote.
>
> Here's some example code:
>
> context.resources.configuration.locales.get (0)

# Code 

Syntax:

<p>`fun main (args: Array <String>) {println ("Hello World!")}`</p>

Result:

`fun main (args: Array <String>) {println ("Hello World!")} `

# Horizontal bar

`hr` is created with three asterisks or three hyphens.

Syntax:

`***`

Result (gray bar):

***

# Links

Syntax:

`This is a built-in [link with title element] (http://example.com/link" I link "). This is [no title] (http://example.com/link).`  

`But [example] [1] [several] [2] [links] [id] with markup like footnotes. Roll and [short record] [] without specifying id.`  

`[1]: http://example.com/" Optional Title Here"`  
`[2]: http://example.com/some`  
`[id]: http://example.com/links (Optional Title Here)`  
`[Short entry]: http://example.com/short`

Result:

This is a built-in [link with title element](http://example.com/link "I am link"). This is [no title](http://example.com/link).  

But [example][1] [several][2] [references][id] with markup like footnotes. [Short entry][] will also work without specifying id.  

[1]: http://example.com/ "Optional Title Here"  
[2]: http://example.com/some  
[id]: http://example.com/links (Optional Title Here)  
[Short entry]: http://example.com/short  

The removal of long urls from the sentence contributes to the preservation of the readability of the source code. Footnotes can be placed anywhere in the document.




# Emphasis

You can highlight words with `*` and `_`. One character for italic text, two characters for bold text, three for italic and bold text at the same time.

Syntax:

`For example, it is _italic_ and it is also *italic*. And this is already __strong__, and so too **strong**. And so ***bold and italic*** at the same time.`

Result:

For example, it is _italic_ and it is also *italic*. And this is already __strong__, and so too **strong**. And so ***bold and italic*** at the same time.

# Strikethrough

Two tildes `~` before and after text.

Syntax:

`~~Strikethrough~~`

Result:

<s>Strikethrough</s>

# Underline

The text should be wrapped with HTML tags `<u>` and `</u>`

Syntax:

`<u>Underlined</u>`

Result:

<u>Underlined</u>

# TODO list

Syntax:

`- [x] item 1`  
`- [] item 2`  
`- [] item 3`