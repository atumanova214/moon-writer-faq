# Toolbar
![](img/selection_toolbar.png)

In order to perform some actions with files and folders, namely:


* rename
* moving
* removal
* restore from backup

**hold your finger on a folder or file**, a **toolbar** will appear at the top, as in the screenshot at the top of the page.

The description of the actions below assumes that the user has already selected the **object** (*is the same file or folder*) in the list.

# Rename objects

To rename a file or folder, click on the three vertical dots at the top of the screen on the toolbar (**Note!** *Files and folders are allowed to be renamed only individually in the amount of **one object per operation!***)
and click on the item **rename**, then a dialog opens in which you can change the name of the selected object.


# Moving objects

To move a file or folder, click the scissors icon **at the top of the screen in the toolbar, then a screen with a list of user files and folders will open, then select where you want to move the selected objects and click the** move **button to move objects** or **cancel button** to cancel the operation.

You can move objects in a batch, i.e. several pieces at once, it does not matter whether these are only folders or files, or all of them together.

# Deleting objects

To delete a file or folder, click on the **garbage bin icon** at the top of the screen on the toolbar, then the deletion process will start, you can cancel it by pressing the **cancel button** on the snack bar, confirm deletion by swiping the snack bar aside or wait when the cancellation time expires and the objects are deleted.  

You can delete objects in batches, i.e. several pieces at once, it does not matter whether these are only folders or files, or all of them together.

# Restore files from a working backup

While working on the file, its backup copies are created (no more than 10, if the threshold of 10 copies per file is exceeded, older ones are automatically deleted).  
A backup is created depending on the threshold set (100 by default *can be changed in the settings*) for actions in the editor.  
An action means removing characters, adding characters, undo/redo actions, inserting text.  
If something happened to the user's file, you can easily restore it if you have a backup copy, for this you need to select the file in the list by long pressing it, click on three points on the toolbar and select **restore the file**, the list of backup files will open copies, the file name will include its identifier and extension, select the necessary backup by clicking on it in the list, after that the file will be restored.  

# Search

Search helps to find files, while the content of files is taken into account in the search process.  
Click on the **magnifying glass** icon. The file list screen will open.  
To start a search operation, enter the required characters in the search field and press **enter** on the keyboard.  