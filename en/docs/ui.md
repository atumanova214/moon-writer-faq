# Main application screen
![](img/1screen.png)

When you start the application, the user gets to the main screen.  
At the top left is the menu button, on the right is the search button for files and folders.  
A round button with a plus is responsible for adding new folders and files.

# Add new folders and files
In order to create a new file or folder, click on the round button with a plus sign, then a window opens in which you will be offered to choose what the user wants to create.
![](img/file_or_folder.png)

# File creation dialog
![](img/file_create_dialog.png)

To create a new file, enter its name and click create.  
Auto-naming creates a file whose name will consist of a date and time (useful for quick notes).  
You can also create a .txt or .md file, tick the necessary extension and create a file by entering its name or press the auto-naming button.

# Folder creation dialog
![](img/folder_create_dialog.png)

To create a new folder enter its name and click create.  
Auto-naming creates a folder whose name will consist of date and time.